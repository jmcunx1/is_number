## is\_number -- Is one of more strings numeric ?

is\_number(1) can be used in shell scripts to determine
if a variable is all numeric.

Repositories:
* [gitlab repository](https://gitlab.com/jmcunx1/is_number) (this site)
* gemini://gem.sdf.org/jmccue/computer/repoinfo/is\_number.gmi (mirror)
* gemini://sdf.org/jmccue/computer/repoinfo/is\_number.gmi (mirror)

[Automake](https://en.wikipedia.org/wiki/Automake)
only confuses me, but this seems to be good enough for me.

To compile:
* If "DESTDIR" is not set, will install under /usr/local
* Execute ./build.sh to create a Makefile,
execute './build.sh --help' for options
* Then make and make install
* Works on Linux, BSD and AIX

To uninstall:
* make (see compile above)
* As root: 'make uninstall' from the source directory

This is licensed using the
[ISC Licence](https://en.wikipedia.org/wiki/ISC_license).
